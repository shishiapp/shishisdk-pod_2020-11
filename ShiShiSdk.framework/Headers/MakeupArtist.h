
#import <UIKit/UIKit.h>
#import <GLKit/GLKit.h>
#import <AVFoundation/AVFoundation.h>

@interface MakeupArtist : NSObject {
#ifdef __cplusplus
    bool isTracking;
    bool inGetTrackingResults;
#endif
}
@property (assign,readonly) BOOL isMakeUpApplied;

@property (nonatomic) BOOL drawsEyeshadow;
@property (nonatomic) BOOL drawsMascara;
@property (nonatomic) BOOL drawsEyeliner;
@property (nonatomic) BOOL drawsGlitter;

@property (nonatomic) BOOL drawsLipstick;
@property (nonatomic) BOOL drawsModel;
@property (nonatomic) BOOL drawsLenses;
@property (nonatomic) BOOL drawsNecklace;
@property (nonatomic) BOOL drawsGlasses;
@property (nonatomic) BOOL compareEnabled;

@property (nonatomic) BOOL drawsFoundation;

@property (nonatomic) BOOL drawsEyebrow;
@property (nonatomic) BOOL removeEyebrow;
@property (nonatomic) BOOL enhancementEnabled;
@property (nonatomic) BOOL smoothEnabled;
@property (nonatomic) BOOL morphEnabled;

@property (nonatomic) BOOL faceGuideEnabled;
    
@property (nonatomic) AVCaptureWhiteBalanceTemperatureAndTintValues temperatureAndTint;


- (void)initTracker;

- (void)tearDownGL;

- (void)startTracker:(BOOL)fromBackCamera;

- (void)startRecording: (NSURL *) movieURL;
- (void)cancelRecording;

- (void)finishRecordingWithCompletionHandler:(void (^)(void))handler;

- (void)setupOpenGLWithContext:(EAGLContext *)openGLContext;

- (void)stopTracker;

- (void)putOnMakeup;

- (void)changeTrackerConfig:(BOOL)auFittingEnabled meshFittingEnabled:(BOOL)meshFittingEnabled;

- (UIImage *)getScreenshotImage:(BOOL)withMakeup;

- (void)setUpEyelinerAssets:(NSArray *) assetsUpper imagesUpper:(NSArray *)imagesUpper assetsLower:(NSArray *)assetsLower imagesLower: (NSArray *)imagesLower;

- (void)setUpEyeliner:(NSArray *)colorsUpper colorsLower:(NSArray *)colorsLower coveragesUpper:(NSArray *)coveragesUpper coveragesLower:(NSArray *)coveragesLower alphaFactorsUpper:(NSArray *)alphaFactorsUpper alphaFactorsLower:(NSArray *)alphaFactorsLower;

- (void)setUpEyeshadowAssets:(NSArray *)assetsUpper imagesUpper:(NSArray *)imagesUpper assetsLower:(NSArray *)assetsLower imagesLower: (NSArray *)imagesLower;

- (void)setUpEyeshadow:(NSArray *)colorsUpper colorsLower:(NSArray *)colorsLower secondColorsUpper:(NSArray *)secondColorsUpper secondColorsLower:(NSArray *)secondColorsLower secondColorAmountsUpper:(NSArray *)secondColorAmountsUpper secondColorAmountsLower:(NSArray *)secondColorAmountsLower coveragesUpper:(NSArray *)coveragesUpper coveragesLower:(NSArray *)coveragesLower glossLevelsUpper:(NSArray *)glossLevelsUpper glossLevelsLower:(NSArray *)glossLevelsLower glossSpreadsUpper:(NSArray *)glossSpreadsUpper glossSpreadsLower:(NSArray *)glossSpreadsLower shimmerLevelsUpper:(NSArray *)shimmerLevelsUpper shimmerLevelsLower:(NSArray *)shimmerLevelsLower sheenLevelsUpper:(NSArray *)sheenLevelsUpper sheenLevelsLower:(NSArray *)sheenLevelsLower glitterLevelsUpper:(NSArray *)glitterLevelsUpper glitterLevelsLower:(NSArray *)glitterLevelsLower glitterAmountsUpper:(NSArray *)glitterAmountsUpper glitterAmountsLower:(NSArray *)glitterAmountsLower glitterSizesUpper:(NSArray *)glitterSizesUpper glitterSizesLower:(NSArray *)glitterSizesLower glitterColorsUpper:(NSArray *)glitterColorsUpper glitterColorsLower:(NSArray *)glitterColorsLower matteLevelsUpper:(NSArray *)matteLevelsUpper matteLevelsLower:(NSArray *)matteLevelsLower alphaFactorsUpper:(NSArray *)alphaFactorsUpper alphaFactorsLower:(NSArray *)alphaFactorsLower;

- (void)setUpMascaraAssets:(NSArray *) assetsUpper imagesUpper:(NSArray *)imagesUpper assetsLower:(NSArray *)assetsLower imagesLower:(NSArray *)imagesLower;

- (void)setUpMascara:(NSArray *)colorsUpper colorsLower:(NSArray *)colorsLower coveragesUpper:(NSArray *)coveragesUpper coveragesLower:(NSArray *)coveragesLower alphaFactorsUpper:(NSArray *)alphaFactorsUpper alphaFactorsLower:(NSArray *)alphaFactorsLower;


- (void)setUpNecklace:(NSString *) asset image:(UIImage *)image offset:(float)offset;

- (void)setUpEyebrowAssets:(NSString *)asset image:(UIImage *)image;

- (void)setUpEyebrow:(CIColor *)color alphaFactor:(float)alphaFactor coverage:(float)coverage translationRatios:(NSArray *)translationRatios scaleRatios:(NSArray *)scaleRatios thickness: (NSInteger)thickness;

- (void)setUpGlassesEnvAsset: (NSString *) envAsset;

- (void)setUpGlassesAssets:(NSArray *) assets images:(NSArray *)images specularAsset:(NSString *) specularAsset specularImage:(UIImage *) specularImage;

- (void)setUpGlasses:(NSArray *)reflectionLevels refractionLevels:(NSArray *)refractionLevels opacityLevels:(NSArray *)opacityLevels specularLevels:(NSArray *)specularLevels colors:(NSArray *)colors secondColors:(NSArray *)secondColors secondColorAmounts:(NSArray *)secondColorAmounts  shininessLevels: (NSArray *)shininessLevels brightnessLevels: (NSArray *)brightnessLevels lightingOn:(bool)lightingOn lightingLevel:(float)lightingLevel shadowLevel:(float)shadowLevel curveFactor1:(float)curveFactor1 curveFactor2:(float)curveFactor2;

-(void)setUpLipAssets:(NSArray *)assets images:(NSArray *)images;

-(void)setUpLip:(NSArray *)colors coverages:(NSArray *)coverages glossLevels:(NSArray *)glossLevels glossSpreads: (NSArray *)glossSpreads shimmerLevels:(NSArray *)shimmerLevels sheenLevels:(NSArray *)sheenLevels glitterLevels:(NSArray *)glitterLevels glitterAmounts:(NSArray *)glitterAmounts matteLevels:(NSArray *)matteLevels sharpEdges:(NSArray *)sharpEdges alphaFactors:(NSArray *)alphaFactors;

-(void)setUpFaceGuide:(NSString *)styleName;

- (BOOL)isTrackerAlive;

-(void)setCompare:(float)position automatic:(BOOL)automatic;

-(void)setUpFoundationAssets:(NSArray *) assets images: (NSArray *) images;

-(void)setUpFoundation:(NSArray *)colors coverages:(NSArray *)coverages lumLevels:(NSArray *)lumLevels shimmerLevels:(NSArray *)shimmerLevels glitterLevels:(NSArray *)glitterLevels glitterAmounts:(NSArray *)glitterAmounts alphaFactors:(NSArray *)alphaFactors;

-(void)setUpSmooth:(NSInteger)strength;

-(void)setUpMorph:(NSInteger)strength;

-(void)setupFilters;

-(void)clear;

-(void) setExpsureBias:(float) value;

@end

